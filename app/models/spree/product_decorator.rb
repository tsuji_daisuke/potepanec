module Spree::ProductDecorator
  def self.prepended(base)
    base.scope :unique, -> (product) { base.included_master.unique_product(product) }

    def base.included_master
      includes(master: [:images, :default_price])
    end

    def base.unique_product(product)
      where.not(id: product.id).distinct
    end
  end

  Spree::Product.prepend self
end
