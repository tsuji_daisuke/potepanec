require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxonomy) { create(:taxonomy, name: "categories") }
  let(:taxon) { create(:taxon, name: "Shirts", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product1) { create(:product, name: "T-shirts", price: "50.00", taxons: [taxon]) }
  let!(:product2) { create(:product, name: "Y-shirts", price: "60.00", taxons: [taxon]) }
  let!(:product3) { create(:product, name: "Polo-shirts", price: "70.00", taxons: [taxon]) }
  let!(:product4) { create(:product, name: "V-neck-shirts", price: "80.00", taxons: [taxon]) }
  let!(:product5) { create(:product, name: "flannel-shirts", price: "90.00", taxons: [taxon]) }
  let!(:product6) { create(:product, name: "sleeveless-shirts", price: "100.00", taxons: [taxon]) }

  describe "商品詳細画面のメイン商品の確認" do
    before do
      get potepan_product_path product1.id
    end

    it 'リクエストは200 OKとなること' do
      expect(response.status).to eq 200
    end

    it 'メイン商品の値段の表示がされていること' do
      expect(response.body).to include product1.price.to_s
    end

    it 'メイン商品の商品名の表示がされていること' do
      expect(response.body).to include product1.name
    end

    it 'メイン商品の商品詳細の表示がされていること' do
      expect(response.body).to include product1.description
    end
  end

  describe "商品詳細画面の関連商品の確認" do
    before do
      get potepan_product_path product1.id
    end

    it '関連商品(product2)が表示がされていること' do
      expect(response.body).to include product2.name
      expect(response.body).to include product2.price.to_s
    end

    it '関連商品(product3)が表示がされていること' do
      expect(response.body).to include product3.name
      expect(response.body).to include product3.price.to_s
    end

    it '関連商品(product4)が表示がされていること' do
      expect(response.body).to include product4.name
      expect(response.body).to include product4.price.to_s
    end

    it '関連商品(product5)が表示がされていること' do
      expect(response.body).to include product5.name
      expect(response.body).to include product5.price.to_s
    end

    it '関連商品(product6)が非表示(関連商品の表示は４つまで)であること' do
      expect(response.body).not_to include product6.name
      expect(response.body).not_to include product6.price.to_s
    end
  end
end
