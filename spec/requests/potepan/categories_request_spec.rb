require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "カテゴリー別商品表示ぺージ(testing_support/factoriesのデータにて検証)" do
    let(:taxonomy1) { create(:taxonomy, name: "categories") }
    let(:taxonomy2) { create(:taxonomy, name: "brand") }
    let(:taxon1) { create(:taxon, name: "Shirts", taxonomy: taxonomy1) }
    let(:taxon2) { create(:taxon, name: "PHP", taxonomy: taxonomy2) }
    let!(:product1) { create(:product, name: "T-shirts", price: "50.00", taxons: [taxon1]) }
    let!(:product2) { create(:product, name: "mug", price: "40.00", taxons: [taxon2]) }

    describe "カテゴリー(taxon)を指定した商品一覧の表示" do
      before do
        get potepan_category_path taxon.id
      end

      context "カテゴリー(taxon)が'Shirts(taxon1)'の場合" do
        let(:taxon) { taxon1 }

        it 'リクエストは200 OKとなること' do
          expect(response.status).to eq 200
        end

        it '商品カテゴリー(taxonomy1)が正しく表示されていること' do
          expect(response.body).to include taxonomy1.name
          expect(response.body).to include taxon1.name
        end

        it '商品カテゴリー(taxonomy2)が正しく表示されていること' do
          expect(response.body).to include taxonomy2.name
          expect(response.body).to include taxon2.name
        end

        it 'product1(T-shirts)が表示されていること' do
          expect(response.body).to include product1.price.to_s
          expect(response.body).to include product1.name
        end

        it 'product2(mug)が非表示になっていること' do
          expect(response.body).not_to include product2.price.to_s
          expect(response.body).not_to include product2.name
        end
      end

      context "カテゴリー(taxon)が'PHP(taxon2)'の場合" do
        let(:taxon) { taxon2 }

        it 'リクエストは200 OKとなること' do
          expect(response.status).to eq 200
        end

        it 'product2(mug)の表示されていること' do
          expect(response.body).to include product2.price.to_s
          expect(response.body).to include product2.name
        end

        it 'product1(T-shirts)の非表示になっていること' do
          expect(response.body).not_to include product1.price.to_s
          expect(response.body).not_to include product1.name
        end
      end
    end
  end
end
