require "spec_helper"

RSpec.describe "application_helper", type: :helper do
  describe "full_title" do
    it ':titleがnilの場合、base_title(BIGBAG Store)のみ表示' do
      expect(helper.full_title).to eq "BIGBAG Store"
    end

    it ':titleが空白の場合、base_title(BIGBAG Store)のみ表示' do
      expect(helper.full_title("")).to eq "BIGBAG Store"
    end

    it 'page_title + base_titleで表示' do
      expect(helper.full_title("Rails bag")).to eq "Rails bag - BIGBAG Store"
    end
  end
end
