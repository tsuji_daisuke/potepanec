require 'rails_helper'

RSpec.feature 'potepan_categories_show', type: :feature do
  let(:taxonomy1) { create(:taxonomy, name: "categories") }
  let(:taxonomy2) { create(:taxonomy, name: "brand") }
  let(:taxon1) { create(:taxon, name: "Shirts", taxonomy: taxonomy1, parent_id: taxonomy1.root.id) }
  let(:taxon2) { create(:taxon, name: "PHP", taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
  let!(:product1) { create(:product, name: "T-shirts", price: "50.00", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "mug", price: "40.00", taxons: [taxon2]) }
  let!(:product3) { create(:product, name: "nothing", price: "30.00") }

  feature 'categories_show.html.erbのリンクの確認' do
    background do
      visit potepan_category_path taxon1.id
    end

    scenario '商品カテゴリーのtaxon1リンクの確認' do
      click_on taxon1.name
      expect(page).to have_current_path potepan_category_path(taxon1.id)
    end

    scenario '商品カテゴリーのtaxon2リンクの確認' do
      click_on taxon2.name
      expect(page).to have_current_path potepan_category_path(taxon2.id)
    end
  end

  feature 'categories_show.html.erbの商品表示件数の確認' do
    scenario '商品カテゴリーがtaxon1の確認' do
      visit potepan_category_path taxon1.id
      expect(all(:css, '.productBox').size).to eq(taxon1.all_products.count)
    end

    scenario '商品カテゴリーがtaxon2の確認' do
      visit potepan_category_path taxon2.id
      expect(all(:css, '.productBox').size).to eq(taxon2.all_products.count)
    end
  end

  feature 'products_show.html.erbの『一覧ページへ戻る』リンクの確認' do
    scenario 'taxon1の確認' do
      visit potepan_category_path taxon1.id
      click_on product1.name
      expect(page).to have_current_path potepan_product_path(product1.id)
      click_on '一覧ページへ戻る'
      expect(page).to have_current_path potepan_category_path(taxon1.id)
    end

    scenario 'taxon2の確認' do
      visit potepan_category_path taxon2.id
      click_on product2.name
      expect(page).to have_current_path potepan_product_path(product2.id)
      click_on '一覧ページへ戻る'
      expect(page).to have_current_path potepan_category_path(taxon2.id)
    end

    scenario 'taxonの紐付きがなく表示されない確認' do
      visit potepan_product_path(product3.id)
      expect(page).to have_current_path potepan_product_path(product3.id)
      has_no_link? '一覧ページへ戻る'
    end
  end
end
