require 'rails_helper'

RSpec.feature 'potepan_product_show', type: :feature do
  feature 'products_show.html.erbリンクの確認' do
    let(:link_taxonomy1) { create(:taxonomy, name: "categories") }
    let(:link_taxon1) { create(:taxon, name: "Shirts", taxonomy: link_taxonomy1) }
    let(:link_product) { create(:product, name: "T-shirts", taxons: [link_taxon1]) }

    background do
      visit potepan_product_path link_product.id
    end

    scenario '/_heder.html.erb のHOMEリンクの確認' do
      within '#headerHomeLink' do
        click_on 'home'
      end
      expect(page).to have_current_path potepan_path
    end

    scenario '_show_page_title.html.erb のHOMEリンクの確認' do
      within '#showPageTitleLink' do
        click_on 'HOME'
      end
      expect(page).to have_current_path potepan_path
    end

    scenario '_show_page_title.html.erb のBIGBAGlogoのリンクの確認' do
      within '.header' do
        click_on 'BIGBAGlogo-home'
      end
      expect(page).to have_current_path potepan_path
    end
  end

  feature 'products_show.html.erbの関連商品の確認' do
    let(:related_taxonomy1) { create(:taxonomy, name: "categories") }
    let(:related_taxonomy2) { create(:taxonomy, name: "brand") }
    let(:related_taxon1) { create(:taxon, name: "Shirts", taxonomy: related_taxonomy1, parent_id: related_taxonomy1.root.id) }
    let(:related_taxon2) { create(:taxon, name: "PHP", taxonomy: related_taxonomy2, parent_id: related_taxonomy2.root.id) }
    let!(:related_product1) { create(:product, name: "T-shirts", price: "50.00", taxons: [related_taxon1]) }
    let!(:related_product2) { create(:product, name: "Y-shirts", price: "30.00", taxons: [related_taxon1]) }
    let!(:related_product3) { create(:product, name: "Polo-shirt", price: "20.00", taxons: [related_taxon1]) }
    let!(:related_product4) { create(:product, name: "mug", price: "40.00", taxons: [related_taxon2]) }
    let!(:related_product5) { create(:product, name: "cup", price: "60.00", taxons: [related_taxon2]) }
    let!(:related_product6) { create(:product, name: "PHP-shirs", price: "40.00", taxons: [related_taxon1, related_taxon2]) }

    context 'メインの商品の商品カテゴリーが１つのみの場合(related_taxon1のみ)' do
      context '商品カテゴリーがrelated_taxon1(Shirts)の場合' do
        background do
          visit potepan_product_path related_product1.id
        end

        scenario '関連商品としてrelated_product2(Y-shirts)の表示とリンクを確認' do
          within '#relatedProductsCount' do
            has_text?(related_product2.price)
            click_on related_product2.name
          end
          expect(page).to have_current_path potepan_product_path(related_product2.id)
        end

        scenario '関連商品としてrelated_product3(Polo-shirt)の表示とリンクを確認' do
          within '#relatedProductsCount' do
            has_text?(related_product3.price)
            click_on related_product3.name
          end
          expect(page).to have_current_path potepan_product_path(related_product3.id)
        end

        scenario '関連商品としてrelated_product6(PHP-shirs)の表示とリンクを確認' do
          within '#relatedProductsCount' do
            has_text?(related_product6.price)
            click_on related_product6.name
          end
          expect(page).to have_current_path potepan_product_path(related_product6.id)
        end
      end

      context '商品カテゴリーがrelated_taxon2(PHP)の場合' do
        background do
          visit potepan_product_path related_product4.id
        end

        scenario '関連商品としてrelated_product5(cup)の表示とリンクを確認' do
          within '#relatedProductsCount' do
            has_text?(related_product5.price)
            click_on related_product5.name
          end
          expect(page).to have_current_path potepan_product_path(related_product5.id)
        end

        scenario '関連商品としてrelated_product6(PHP-shirs)の表示とリンクを確認' do
          within '#relatedProductsCount' do
            has_text?(related_product6.price)
            click_on related_product6.name
          end
          expect(page).to have_current_path potepan_product_path(related_product6.id)
        end
      end
    end

    context 'メインの商品の商品カテゴリーが2つ(Shirts,PHP)の場合' do
      background do
        visit potepan_product_path related_product6.id
      end

      scenario '関連商品としてrelated_product1(T-shirts)の表示とリンクを確認' do
        within '#relatedProductsCount' do
          has_text?(related_product1.price)
          click_on related_product1.name
        end
        expect(page).to have_current_path potepan_product_path(related_product1.id)
      end

      scenario '関連商品としてrelated_product2(Y-shirts)の表示とリンクを確認' do
        within '#relatedProductsCount' do
          has_text?(related_product2.price)
          click_on related_product2.name
        end
        expect(page).to have_current_path potepan_product_path(related_product2.id)
      end

      scenario '関連商品としてrelated_product3(Polo-shirt)の表示とリンクを確認' do
        within '#relatedProductsCount' do
          has_text?(related_product3.price)
          click_on related_product3.name
        end
        expect(page).to have_current_path potepan_product_path(related_product3.id)
      end

      scenario '関連商品としてrelated_product4(mug)の表示とリンクを確認' do
        within '#relatedProductsCount' do
          has_text?(related_product4.price)
          click_on related_product4.name
        end
        expect(page).to have_current_path potepan_product_path(related_product4.id)
      end
    end
  end
end
